Exemplos - Laboratório
---

**ADS15 Módulo 4** - Gerência de container utilizando Docker e Kubernetes

## Exempos

1. exemplo1 - Utilizando Service (Service: NodePort)
2. exemplo2 - Utilizando Ingress (Service: ClusterIP)
3. exemplo3 - Utilizando ReplicaSet
4. exemplo4 - Utilizando Deployment
5. exemplo5 - Utilizando PVC/PV



## Comandos

### Criação do Cluster
Iniciar o minikube em uma máquina virtual

```bash
minikube start --driver=virtualbox
```

### Coletar status

Coletar o status dos objetos criados no cluster

```bash
kubectl get all
```

Coletar os status separadamente dos objetos

```bash
# service
kubectl get service

# pod
kubectl get pod

# ingress
kubectl get ingress

# deployment
kubectl get deployment

# replicaset
kubectl get replicaset

# pv
kubectl get pv

# pvc
kubectl get pvc
```

Ficar checando o status dos objetos

```bash
watch -n 0.5 kubectl get all
```

> O comando "kubectl get pod" possui a opção -w responsável em ficar atualizando as informações na tela.

### Criação de Objetos

Considerando que os arquivos com nossos objetos chamam-se "foo.yaml", temos:

Criar um objeto

```bash
kubectl apply -f foo.yaml
``` 

> TODOS os objetos do kubernetes podem estar em apenas um arquivo .yaml, para isso, a cada objeto deve ser separado por "---"

Arquivo compacto com mais de um objeto

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: nginx

---
apiVersion: v1
kind: Service
metadata:
  name: myapp-service
spec:
  selector:
    app: myapp
  type: NodePort
  ports:
  - targetPort: 80
    port: 80
    nodePort: 30008
...

```

> Notem: Um arquivo YAML deve começar com "---" e finalizar com "...".
